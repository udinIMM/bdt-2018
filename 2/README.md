# Implementasi Partisi Basis Data

05111540000088 - Ilham Muhammad Misbahuddin

## Outline
- [Deskripsi Server](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#deskripsi-server-yang-digunakan)
- [Implementasi Partisi 1: Sakila DB](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#implementasi-partisi-1-sakila-db)
    - [Deskripsi dataset](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#deskripsi-dataset)
    - [Proses pembuatan partisi](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#proses-pembuatan-partisi)
    - [Benchmarking](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#benchmarking)
- [Implementasi Partisi 2: Measures dataset](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#implementasi-partisi-2-measures-dataset)
    - [Deskripsi dataset](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#deskripsi-dataset-1)
    - [Import Dataset](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#import-dataset)
    - [Benchmarking](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#benchmarking-1)
- [Kesimpulan](https://gitlab.com/udinIMM/bdt-2018/tree/master/2#kesimpulan)
  
## Deskripsi Server yang digunakan
- Sistem operasi: Linux Ubuntu 18.04.1 LTS
- Versi MySQL: MySQL 5.7.23
- RAM: 8 GB
- CPU: 4 Cores

## Implementasi Partisi 1: Sakila DB

### Deskripsi dataset
- Pada kasus ini dataset yang digunakan adalah Sakila DB dan dapat diunduh pada [http://downloads.mysql.com/docs/sakila-db.zip](http://downloads.mysql.com/docs/sakila-db.zip).
- Impor database dapat menggunakan
    ```shell
    mysql -u root -p
    ```
    ```sql
    mysql> SOURCE /path/to/file;
    ```
    Contoh:
    ```sql
    mysql> SOURCE /home/administrator/Downloads/sakila-db/sakila-schema.sql;
    mysql> SOURCE /home/administrator/Downloads/sakila-db/sakila-data.sql;
    ```
- Dataset ini terdiri dari **23 tabel** dengan masing-masing tabel memiliki jumlah baris data sebagai berikut:
![sakila rows](img/001.png)
- Perintah untuk menampilkan jumlah baris data pada masing-masing tabel.
    ```sql
    select table_name, table_rows from information_schema.tables where table_schema = 'sakila';
    ```

### Proses pembuatan partisi
- Pemilihan tabel yang akan dipartisi
  Pemilihan tabel yang akan dipartisi ditentukan berdasarkan jumlah data yang paling banyak dari keseluruhan tabel dan memungkinkan untuk data tersebut bertambah secara signifikan. Pada kasus sakila DB ini, tabel yang terpilih untuk dipartisi yaitu tabel payment dan tabel rental.
- Daftar tabel yang akan dipartisi:
    - Tabel ***Payment***.
    - Tabel ***Rental***

#### Tabel Payment  
- Jenis partisi yang digunakan: **HASH**.
- Sehingga tidak perlu menentukan predikat partisi, maka tabel tersebut akan dibagi menjadi **5 bagian** yaitu:
    - p<sub>0</sub> = Data dengan payment_id berakhiran 0 dan 5.
    - p<sub>0</sub> = Data dengan payment_id berakhiran 1 dan 6.
    - p<sub>0</sub> = Data dengan payment_id berakhiran 2 dan 7.
    - p<sub>0</sub> = Data dengan payment_id berakhiran 3 dan 8.
    - p<sub>0</sub> = Data dengan payment_id berakhiran 4 dan 9.

#### Implementasi Partisi
- Script SQL yang dapat digunakan untuk mempartisi tabel **Payment**.
    ```sql
    CREATE TABLE payment (
        payment_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
        customer_id SMALLINT UNSIGNED NOT NULL,
        staff_id TINYINT UNSIGNED NOT NULL,
        rental_id INT DEFAULT NULL,
        amount DECIMAL(5,2) NOT NULL,
        payment_date DATETIME NOT NULL,
        last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY  (payment_id),
        KEY idx_fk_staff_id (staff_id),
        KEY idx_fk_customer_id (customer_id)
    --  CONSTRAINT fk_payment_rental FOREIGN KEY (rental_id) REFERENCES rental (rental_id) ON DELETE SET NULL ON UPDATE CASCADE,
    --  CONSTRAINT fk_payment_customer FOREIGN KEY (customer_id) REFERENCES customer (customer_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    --  CONSTRAINT fk_payment_staff FOREIGN KEY (staff_id) REFERENCES staff (staff_id) ON DELETE RESTRICT ON UPDATE CASCADE
    )ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ALTER TABLE payment
        PARTITION BY HASH (payment_id)
        PARTITIONS 5;
    ```
- Script SQL yang dapat digunakan untuk mempartisi tabel **Rental**.
    ```sql
    CREATE TABLE rental (
        rental_id INT NOT NULL AUTO_INCREMENT,
        rental_date DATETIME NOT NULL,
        inventory_id MEDIUMINT UNSIGNED NOT NULL,
        customer_id SMALLINT UNSIGNED NOT NULL,
        return_date DATETIME DEFAULT NULL,
        staff_id TINYINT UNSIGNED NOT NULL,
        last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (rental_id),
        UNIQUE KEY  (rental_id,staff_id,inventory_id,customer_id),	
        KEY idx_fk_inventory_id (inventory_id),
        KEY idx_fk_customer_id (customer_id),
        KEY idx_fk_staff_id (staff_id)
    --  CONSTRAINT fk_rental_staff FOREIGN KEY (staff_id) REFERENCES staff (staff_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    --  CONSTRAINT fk_rental_inventory FOREIGN KEY (inventory_id) REFERENCES inventory (inventory_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    --  CONSTRAINT fk_rental_customer FOREIGN KEY (customer_id) REFERENCES customer (customer_id) ON DELETE RESTRICT ON UPDATE CASCADE
    )ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ALTER TABLE rental 
        PARTITION BY HASH(rental_id)
        PARTITIONS 5;
    ```

### Benchmarking
- Insert data baru ke dalam tabel.
    ```sql
    insert into sakila.payment (customer_id, staff_id, rental_id, amount, payment_date) values
    (118,2,null,'5.66','2018-09-04 09:20:00'),
    (118,2,null,'2.73','2018-09-05 10:10:00'),
    (118,2,null,'4.69','2018-09-07 10:30:00'),
    (118,2,null,'4.41','2018-09-10 11:20:00'),
    (118,2,null,'3.15','2018-09-11 13:00:00'),
    (118,2,null,'3.05','2018-09-14 13:20:00'),
    (118,1,null,'5.30','2018-09-17 15:25:00'),
    (118,1,null,'1.76','2018-09-24 16:30:00'),
    (118,1,null,'1.29','2018-09-26 16:40:00'),
    (118,1,null,'5.58','2018-09-27 17:00:00');
    ```
    Hasilnya:<br>
    ![Insert data baru ke dalam tabel](img/002.png)<br>
- Select data dari partisi yang benar.
    ```sql
    select * from sakila.payment partition(p0) where payment_id = 16050 limit 10;
    ```
    Hasilnya:<br>
    ![Select data dari partisi yang benar](img/003.png)<br>
        - Data ada di partisi tersebut.
- Select data dari partisi yang salah.
    ```sql
    select * from sakila.payment partition(p1) where payment_id = 16050 limit 10;
    ```
    Hasilnya:<br>
    ![Select data dari partisi yang salah](img/004.png)<br>
        - Data tidak ada pada partisi tersebut.

## Implementasi Partisi 2: Measures Dataset

### Deskripsi Dataset
- Dataset ini terdiri dari **2 tabel**, yaitu ***measures*** dan ***partitioned_measures***.
- Sumber dataset:
    - [https://drive.google.com/file/d/0B2Ksz9hP3LtXRUppZHdhT1pBaWM/view](https://drive.google.com/file/d/0B2Ksz9hP3LtXRUppZHdhT1pBaWM/view)

### Import Dataset
- Langkah pertama, unduh terlebih dahulu dataset yang telah disediakan pada sumber di atas
- Langkah kedua, gunakan perintah berikut untuk mengakses MySQL.
    ```shell
    mysql -u root -p
    ```
- Langkah ketiga, buat database bernama measures.
    ```sql
    mysql> CREATE DATABASE measures;
    ```
- Langkah keempat, gunakan database measures.
    ```sql
    mysql> USE measures;
    ```
- Langkah kelima, impor database yang sudah diunduh tadi.
    ```sql
    mysql> SOURCE /path/to/file;
    ```
    Contoh:
    ```sql
    mysql> SOURCE /home/administrator/Downloads/sample_1_8_M_rows_data.sql;
    ```

### Benchmarking
- Lakukan benchmarking yang sama dengan ada di dalam tutorial.
- Lakukan masing-masing sebanyak 10x dan hitung rata-rata waktu eksekusinya.
- Berikan kesimpulan manakah yang lebih cepat: tabel yang dipartisi ataukah tabel yang tidak dipartisi.

#### Select Benchmark
| No. Pengujian | Tabel tanpa partisi (detik) | Tabel dengan partisi (detik) |
|---------------|-----------------------------|------------------------------|
|       1       |          3,96 detik         |          0,73 detik          |
|       2       |          4,01 detik         |          0,74 detik          |
|       3       |          4,00 detik         |          0,73 detik          |
|       4       |          4,01 detik         |          0,74 detik          |
|       5       |          4,02 detik         |          0,74 detik          |
|       6       |          4,00 detik         |          0,73 detik          |
|       7       |          3,93 detik         |          0,73 detik          |
|       8       |          3,97 detik         |          0,73 detik          |
|       9       |          3,93 detik         |          0,74 detik          |
|      10       |          4,00 detik         |          0,74 detik          |
|   Rata-rata   |          3,983 detik        |          0,735 detik         |

#### Big Delete Benchmark
| No. Pengujian | Tabel tanpa partisi (detik) | Tabel dengan partisi (detik) |
|---------------|-----------------------------|------------------------------|
|       1       |          2,36 detik         |          0,38 detik          |
|       2       |          2,08 detik         |          0,32 detik          |
|       3       |          2,18 detik         |          0,22 detik          |
|       4       |          2,40 detik         |          0,23 detik          |
|       5       |          2,03 detik         |          0,21 detik          |
|       6       |          2,09 detik         |          0,23 detik          |
|       7       |          2,06 detik         |          0,22 detik          |
|       8       |          2,02 detik         |          0,23 detik          |
|       9       |          2,13 detik         |          0,23 detik          |
|      10       |          2,06 detik         |          0,23 detik          |
|   Rata-rata   |          2,141 detik        |          0,25 detik          |

## Kesimpulan
- Dari percobaan select benchmark dapat disimpulkan bahwa select dari tabel yang dipartisi akan lebih cepat dari pada select dari tabel tanpa dipartisi. Kecepatan select dari tabel tanpa dipartisi 5 kali lebih lambat dari pada select tabel yang dipartisi.
- Dari percobaan big delete benchmark dapat disimpulkan bahwa delete data yang banyak akan lebih cepat menggunakan tabel yang dipartisi karena cukup dengan drop partisi yang ingin dihapus. Kecepatan delete data dari tabel tanpa dipartisi hampir 10 kali lebih lambat dari pada delete data dari tabel yang dipartisi.