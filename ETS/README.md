# Implementasi Parallel DBMS dengan MySQL Cluster (ETS)

05111540000088 - Ilham Muhammad Misbahuddin

## Outline
- [Tujuan](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#tujuan)
- [Deskripsi Sistem](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#deskripsi-sistem)
    - [Arsitektur Sistem](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#arsitektur-sistem)
    - [MySQL Cluster](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#mysql-cluster) 
    - [ProxySQL](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#proxysql)
- [Implementasi Sistem](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#implementasi-sistem)
- [Tes Koneksi](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#tes-koneksi)
- [Referensi](https://gitlab.com/udinIMM/bdt-2018/tree/master/ETS#referensi)

## Tujuan
- Pada tugas ini, mahasiswa diharapkan mampu untuk :
    1. Membuat **server basis data terdistribusi** dengan menggunakan konsep **group replication**.
    2. Mampu menambahkan **load balancer** (ProxySQL) untuk membagi **request ke server basis data**.
    3. **Menambahkan aplikasi** CMS (Wordpress) yang memanfaatkan arsitektur tersebut.
    4. Menguji kehandalan sistem (testing) dengan **menyimulasikan matinya beberapa node** dan menunjukkan bahwa **data tetap tereplikasi** pada node-node server basis data.

## Deskripsi Sistem

### Arsitektur Sistem
![Arsitektur Sistem](img/arsitektur.png)

### MySQL Cluster
- Sistem Operasi : Ubuntu 16.04
- RAM : 512 MB
- IP Lokal : 192.168.33.11 (**db1**), 192.168.33.12 (**db2**), dan 192.168.33.13 (**db3**).
- MySQL Server : menggunakan **community edition** yang support **group replication**
    - mysql-common_5.7.23
    - mysql-community-client_5.7.23
    - mysql-client_5.7.23
    - mysql-community-server_5.7.23
- Konfigurasi Basis Data :
    - cluster_bootstrap.sql : melakukan bootstrapping MySQL group replication.
    - cluster_member.sql : melakukan konfigurasi group replication pada node yang lain.
    - addition_to_sys.sql : patch script untuk ProxySQL.
    - create_proxysql_user.sql : create user untuk ProxySQL ('monitor' untuk monitoring, 'playgrounduser' untuk contoh aplikasi).
 
### ProxySQL
- Sistem Operasi : Ubuntu 16.04
- RAM : 512 MB
- IP Lokal : 192.168.33.10
- MySQL Client :
    - mysql-common_5.7.23
    - mysql-community-client_5.7.23
    - mysql-client_5.7.23
- Konfigurasi basis data :
    - proxysql.sql : mengubah user admin ProxySQL menambahkan user 'monitoring', menambahkan node MySQL, menambahkan user 'playground'.

## Implementasi Sistem

### Persiapan
- Instalasi vagrant dan virtualbox
    ```shell
    $ sudo apt-get install vagrant virtualbox
    ```
- Ekstrak file ZIP yang telah disediakan ke dalam sebuah direktori & buka direktori tersebut.
    ```shell
    $ unzip mysql-cluster-proxysql.zip
    $ cd mysql-cluster-proxysql
    ```

### Modifikasi Vagrantfile
- Lakukan perubahan pada **Vagrantfile** dengan menggunakan text editor :
    ```shell
    # -*- mode: ruby -*-
    # vi: set ft=ruby :

    # All Vagrant configuration is done below. The "2" in Vagrant.configure
    # configures the configuration version (we support older styles for
    # backwards compatibility). Please don't change it unless you know what
    # you're doing.

    Vagrant.configure("2") do |config|
    
        # MySQL Cluster dengan 3 node
        (1..3).each do |i|
            config.vm.define "db#{i}" do |node|
            node.vm.hostname = "db#{i}"
            node.vm.box = "bento/ubuntu-16.04"
            node.vm.network "private_network", ip: "192.168.33.1#{i}"

            # Opsional. Edit sesuai dengan nama network adapter di komputer
            node.vm.network "public_network", bridge: "eno1"
            
            node.vm.provider "virtualbox" do |vb|
                vb.name = "db#{i}"
                vb.gui = false
                vb.memory = "512" # Disesuaikan dengan kemampuan RAM pada Host
            end
        
            node.vm.provision "shell", path: "deployMySQL1#{i}.sh", privileged: false
            end

            # Digunakan ketika terjadi error pada saat Connection Reset. Retrying...
            config.ssh.insert_key = false
        end

        config.vm.define "proxy" do |proxy|
            proxy.vm.hostname = "proxy"
            proxy.vm.box = "bento/ubuntu-16.04"
            proxy.vm.network "private_network", ip: "192.168.33.10"
            proxy.vm.network "public_network",  bridge: "eno1"
            
            proxy.vm.provider "virtualbox" do |vb|
            vb.name = "proxy"
            vb.gui = false
            vb.memory = "512" # Disesuaikan dengan kemampuan RAM pada Host
            end

            proxy.vm.provision "shell", path: "deployProxySQL.sh", privileged: false
        end

        # Digunakan ketika terjadi error pada saat Connection Reset. Retrying...
        config.ssh.insert_key = false
    end
    ```
    Pada **Vagrantfile** ini dilakukan 3 perubahan :
    - Mengurangi memori yang dialokasikan untuk setiap VM, dari 1024 MB menjadi 512 MB. Hal ini bertujuan agar tidak membebani host ketika VM dijalankan. 
    - Menambahkan `config.ssh.insert_key = false` agar tidak terjadi error ketika vagrant melakukan insert key untuk SSH secara otomatis.
    - Mengatur network adapter sesuai dengan yang digunakan oleh host, misal `eno1`.

### Provisioning
- Ada 4 tahap provisioning yang dilakukan, yaitu :
    1. Provisioning pada host **db1** (tidak ada modifikasi).
    2. Provisioning pada host **db2** (tidak ada modifikasi).
    3. Provisioning pada host **db3** (tidak ada modifikasi).
    4. Provisioning pada hsot **proxy** (ada modifikasi).
- MySQL Cluster server (**db1**) :
    File `deployMySQL11.sh` tidak ada perubahan
    ```shell
    sudo apt-get update
    sudo apt-get install libaio1
    sudo apt-get install libmecab2
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
    sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/root-pass password admin'
    sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/re-root-pass password admin'
    sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
    sudo ufw allow 33061
    sudo ufw allow 3306
    sudo cp /vagrant/my11.cnf /etc/mysql/my.cnf
    sudo service mysql restart
    sudo mysql -u root -padmin < /vagrant/cluster_bootstrap.sql
    sudo mysql -u root -padmin < /vagrant/addition_to_sys.sql
    sudo mysql -u root -padmin < /vagrant/create_proxysql_user.sql
    ```
- MySQL Cluster server (**db2**) :
    File `deployMySQL12.sh` tidak ada perubahan
    ```shell
    sudo apt-get update
    sudo apt-get install libaio1
    sudo apt-get install libmecab2
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
    sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/root-pass password admin'
    sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/re-root-pass password admin'
    sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
    sudo ufw allow 33061
    sudo ufw allow 3306
    sudo cp /vagrant/my12.cnf /etc/mysql/my.cnf
    sudo service mysql restart
    sudo mysql -u root -padmin < /vagrant/cluster_member.sql
    ```
- MySQL Cluster server (**db3**) :
    File `deployMySQL13.sh` tidak ada perubahan
    ```shell
    sudo apt-get update
    sudo apt-get install libaio1
    sudo apt-get install libmecab2
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
    sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/root-pass password admin'
    sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/re-root-pass password admin'
    sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
    sudo ufw allow 33061
    sudo ufw allow 3306
    sudo cp /vagrant/my13.cnf /etc/mysql/my.cnf
    sudo service mysql restart
    sudo mysql -u root -padmin < /vagrant/cluster_member.sql
    ```
- ProxySQL server :
    Lakukan perubahan pada file `deployProxySQL.sh` dengan menggunakan text editor menjadi sebagai berikut :
    ```shell
    sudo apt-get update
    cd /tmp
    curl -OL https://github.com/sysown/proxysql/releases/download/v1.4.4/proxysql_1.4.4-ubuntu16_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb

    sudo apt-get install libaio1
    sudo apt-get install libmecab2
    sudo dpkg -i proxysql_1.4.4-ubuntu16_amd64.deb
    sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb

    sudo ufw allow 33061
    sudo ufw allow 3306

    sudo systemctl start proxysql
    #mysql -u admin -padmin -h 127.0.0.1 -P 6032 < /vagrant/proxysql.sql

    # Instalasi apache2 dan php
    sudo apt-get install -y apache2 php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-mbstring php7.0-gd php7.0-xml php7.0-xmlrpc php7.0-intl php7.0-soap php7.0-zip
    sudo systemctl start apache2

    # Instalasi wordpress
    cd /tmp
    wget -c http://wordpress.org/latest.tar.gz
    tar -xzvf latest.tar.gz
    sudo mv wordpress/* /var/www/html
    sudo cp /vagrant/wp-config.php /var/www/html/ # Menambahkan file wp-config.php
    sudo chown -R www-data:www-data /var/www/html
    sudo chmod -R 755 /var/www/html
    sudo mv /var/www/html/index.html /var/www/html/index.html.bak
    sudo service apache2 restart
    ```
- Melakukan perubahan pada file wp-config.php yang telah disediakan oleh wordpress menjadi seperti berikut:
    ```php
    <?php
    /**
    * The base configuration for WordPress
    *
    * The wp-config.php creation script uses this file during the
    * installation. You don't have to use the web site, you can
    * copy this file to "wp-config.php" and fill in the values.
    *
    * This file contains the following configurations:
    *
    * * MySQL settings
    * * Secret keys
    * * Database table prefix
    * * ABSPATH
    *
    * @link https://codex.wordpress.org/Editing_wp-config.php
    *
    * @package WordPress
    */

    // ** MySQL settings - You can get this info from your web host ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'wordpress');

    /** MySQL database username */
    define('DB_USER', 'wordpress');

    /** MySQL database password */
    define('DB_PASSWORD', 'wordpress');

    /** MySQL hostname */
    define('DB_HOST', '192.168.33.10:6033');

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');

    /**#@+
    * Authentication Unique Keys and Salts.
    *
    * Change these to different unique phrases!
    * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
    * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
    *
    * @since 2.6.0
    */
    define('AUTH_KEY',         'put your unique phrase here');
    define('SECURE_AUTH_KEY',  'put your unique phrase here');
    define('LOGGED_IN_KEY',    'put your unique phrase here');
    define('NONCE_KEY',        'put your unique phrase here');
    define('AUTH_SALT',        'put your unique phrase here');
    define('SECURE_AUTH_SALT', 'put your unique phrase here');
    define('LOGGED_IN_SALT',   'put your unique phrase here');
    define('NONCE_SALT',       'put your unique phrase here');

    /**#@-*/

    /**
    * WordPress Database Table prefix.
    *
    * You can have multiple installations in one database if you give each
    * a unique prefix. Only numbers, letters, and underscores please!
    */
    $table_prefix  = 'wp_';

    /**
    * For developers: WordPress debugging mode.
    *
    * Change this to true to enable the display of notices during development.
    * It is strongly recommended that plugin and theme developers use WP_DEBUG
    * in their development environments.
    *
    * For information on other constants that can be used for debugging,
    * visit the Codex.
    *
    * @link https://codex.wordpress.org/Debugging_in_WordPress
    */
    define('WP_DEBUG', false);

    /* That's all, stop editing! Happy blogging. */

    /** Absolute path to the WordPress directory. */
    if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

    /** Sets up WordPress vars and included files. */
    require_once(ABSPATH . 'wp-settings.php');
    ```
### Menjalankan Vagrant
- Setelah melakukan modifikasi seperti di atas, jalankan vagrant dengan perintah berikut :
    ```shell
    $ vagrant up
    ```
- Tunggu selama beberapa menit. Setelah itu cek apakah VM sudah berjalan dengan perintah berikut :
    ```shell
    $ vagrant status
    ```
    ![Vagrant status](img/001.png)
- Selanjutnya jalankan *provision* tambahan untuk ProxySQL dengan ssh ke ProxySQL server terlebih dahulu.
    ```shell
    $ vagrant ssh proxy
    ```
- Jalankan :
    ```shell
    (proxy)$ mysql -u admin -p -h 127.0.0.1 -P 6032 < /vagrant/proxysql.sql
    # password = admin
    ```
- Tes koneksi ke ProxySQL dari group MySQL. Dari salah satu node (di contoh ini **db1**). Jalankan :
    ```shell
    (db1)$ mysql -u playgrounduser -p -h 192.168.33.10 -P 6033
    # password = playgroundpassword
    ```

### Konfigurasi Wordpress
- Menyediakan database yang akan digunakan oleh wordpress dengan melakukan ssh ke server **db1** terlebih dahulu.
    ```shell
    $ vagrant ssh db1
    ```
- Masuk ke mysql.
    ```shell
    (db1)$ mysql -u root -p
    # password = admin
    ```
- Buat *database* baru.
    ```sql
    (db1) mysql> CREATE DATABASE wordpress;
    ```
- Buat *user* baru untuk wordpress.
    ```sql
    (db1) mysql> CREATE USER 'wordpress'@'%' IDENTIFIED BY 'wordpress';
    (db1) mysql> GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%';
    (db1) mysql> FLUSH PRIVILEGES;
    ```
    *User* baru sudah berhasil dibuat. *User* dan *database* ini juga otomatis terreplikasi di MySQL Cluster member, yaitu server **db2** dan **db3**.
    ![Replikasi berhasil](img/002.png)
- Selanjutnya, tambahkan *user* yang sama ke server ProxySQL dengan masuk ke mysql terlebih dahulu.
    ```sql
    (proxy)$ mysql -u admin -p -h 127.0.0.1 -P 6032
    # password = password
    ```
- Buat *user* baru untuk wordpress dengan *password* yang sama juga.
    ```sql
    (proxy) mysql> INSERT INTO mysql_users(username, password, default_hostgroup) VALUES ('wordpress', 'wordpress', 2);
    (proxy) mysql> LOAD MYSQL USERS TO RUNTIME;
    (proxy) mysql> SAVE MYSQL USERS TO DISK;
    ```
- Kunjungi alamat IP ProxySQL (http://192.168.33.10) pada browser untuk mensikronisasi aplikasi wordpress dengan database yang telah dibuat sebelumnya. Ketika dikunjungi maka tampilan awal dari wordpress adalah sebagai berikut :
    ![Pilih bahasa](img/003.png)
- Pilih bahasa yang ingin digunakan lalu klik **Continue** dan akan diminta untuk mengisi informasi yang diperlukan ketika login.
    ![Form informasi login](img/004.png)
- Jika informasi yang diisikan sudah benar, maka ketika ditekan tombol **Install Wordpress** akan muncul halaman seperti berikut :
    ![Instal sukses](img/005.png)
- Cara lain Untuk memastikan apakah wordpress sudah berhasil terinstal adalah mengunjungi alamat IP ProxySQL (http://192.168.33.10) kembali. Maka tampilannya akan berubah menjadi seperti berikut :
    ![Wordpress BDT](img/008.png)
- Lalu langkah selanjutnya adalah mencoba login.
    ![Login wordpress](img/006.png)
- Jika berhasil login, maka akan menuju halaman *dashboard*
    ![Dashboard wordpress](img/007.png)

## Tes Koneksi

### Tes Koneksi
- Pastikan dapat terkoneksi dengan layanan SSH pada semua VM terlebih dahulu. Perintah yang digunakan adalah sebagai berikut dan dilakukan pada 4 terminal berbeda.
    ```shell
    $ vagrant ssh db1
    $ vagrant ssh db2
    $ vagrant ssh db3
    $ vagrant ssh proxy
    ```
    ![Berhasil ssh](img/009.png)
- Pastikan *user* dapat terkoneksi ke Group MySQL dari VM ProxySQL.
    ```shell
    mysql -u playgrounduser -p -h 127.0.0.1 -P 6033 --prompt='ProxySQLClient> '
    # password = playgroundpassword
    ```
    ![ProxySQLClient berhasil](img/010.png)

### Tes Replikasi
- Login ke MySQL pada Group Replication Host dengan perintah berikut :
    ```shell
    $ mysql -uroot -padmin
    ```
- Login ke MySQL pada ProxySQL menggunakan *user* wordpress dengan perintah berikut :
    ```shell
    $ mysql -u wordpress -p -h 127.0.0.1 -P 6033 --prompt='ProxySQLClient> '
    # password = wordpress
    ```
- Lakukan query dengan perintah berikut :
    ```sql
    mysql> select * from wordpress.wp_users;
    ```
    Maka hasilnya akan seperti gambar berikut :
    ![Replikasi berhasil](img/011.png)

### Tes ProxySQL
- Pengujian yang dilakukan adalah dengan cara mematikan salah satu node MySQL Server (**db1**, **db2** atau **db3**) dan pada kasus ini VM **db3** yang dimatikan dengan menggunakan perintah sebagai berikut :
    ```shell
    (db3)$ sudo systemctl stop mysql
    (db3)$ sudo systemctl status mysql # untuk mengetahui status
    ```
    ![Mematikan mysql db3](img/012.png)
- Lakukan pengecekan pada ProxySQL admin dengan perintah sebagai berikut :
    ```shell
    (proxy)$ mysql -u admin -p -h 127.0.0.1 -P 6032 --prompt='ProxySQLAdmin> '
    ```
    Lalu dilanjutkan dengan menggunakan perintah pada mysql seperti berikut :
    ```sql
    (proxy) mysql> SELECT hostgroup_id, hostname, status FROM runtime_mysql_servers;
    ```
    ![Cek ProxySQL admin](img/013.png)
- Melakukan posting pada wordpress.
    ![Buat posting baru](img/014.png)
- Menyalakan mysql yang tadi telah dimatikan dengan perintah berikut :
    ```shell
    (db3)$ sudo systemctl start mysql
    ```
- Lakukan pengecekan apakah postingan berhasil direplikasi atau tidak.
    ```sql
    (db3) mysql> use wordpress;
    (db3) mysql> select * from wp_posts;
    ```
    ![Pengecekan postingan](img/015.png)
    Berikut hasil dari postingan yang telah berhasil direplikasi pada VM db3.
    ![Pengecekan postingan](img/016.png)

## Referensi
- [How to Configure MySQL Group Replication on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-configure-mysql-group-replication-on-ubuntu-16-04)
- [How to Use ProxySQL as a Load Balancer for MySQL on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-use-proxysql-as-a-load-balancer-for-mysql-on-ubuntu-16-04#step-8-%E2%80%94-verifying-the-proxysql-configuration)
